import React, {Component}  from 'react';
import { connect } from 'react-redux';
// import { selectFish, addFish, removeFish } from '../actions/index';
import { bindActionCreators } from 'redux';

import * as Actions from '../actions/index';
import CreateFish from './create_fish';

require('../../style/inventory.scss');

 class Inventory extends Component{

  // constructor(props, context) {
  //   super(props, context);
  //
  //   this.actions = bindActionCreators(Actions, this.props.dispatch);
  // }

  renderList(){
    // console.log(this.props);
    return this.props.fishes.items.map( fish => {
      return (
        <div
        key={fish.name}
        // onClick={ () => this.props.selectBook(book) }
        className="item">
          <div className="row">
            <input className="col col-md-4" value={fish.name} />
            <input className="col col-md-4" value={fish.price} />
            <input className="col col-md-4" value={fish.status} />
          </div>
          <div className="row">
            <input className="col desc col-md-12" value={fish.desc} />
          </div>
          <div className="row">
            <input className="col desc col-md-12" value={fish.image} />
          </div>
          <div className="row">
            <button className="col desc col-md-12" onClick={ () => this.props.removeFish(fish) }> REMOVE FISH </button>
          </div>

        </div>
      );
    });
  }



  render(){
    return (
      <div className="inventory col-md-4">
        <h3 className="title">Inventory</h3>
        {this.renderList()}
        <CreateFish />
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    fishes: state.fishes,
    activeFish: state.activeFish,
  };
}

function mapDispatchToProps(dispatch){

  return bindActionCreators({
    removeFish: Actions.removeFish,
    addFish: Actions.addFish,
   }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Inventory);
