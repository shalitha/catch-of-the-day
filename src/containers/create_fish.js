import React, {Component}  from 'react';
import { connect } from 'react-redux';
// import { selectFish, addFish, removeFish } from '../actions/index';
import { bindActionCreators } from 'redux';

import * as Actions from '../actions/index';

require('../../style/inventory.scss');

 class CreateFish extends Component{

   initialState = {
    name: null,
    price: null,
    status: null,
    desc: null,
    image: null
  };

  constructor(props){
    super(props);

    this.state = this.initialState;
  }

  onAddButtonClick(){
    this.props.addFish(this.state);
    this.resetState();
  }

  onBlur(property, event){
    this.setState({ [property]: event.target.value });
  }

  resetState(){
    this.setState(this.initialState);
  }


  renderNewItemForm(){
    // console.log(this.props);
    // return this.props.fishes.items.map( fish => {
      return (
        <div className="item col col-md-12">
          <div className="row">
            <input className="col col-md-4" value={this.state.name} onBlur={ (event) => this.onBlur('name', event) } />
            <input className="col col-md-4" value={this.state.price} onBlur={ (event) => this.onBlur('price', event) } />
            <select className="col col-md-4" value={this.state.status} onBlur={ (event) => this.onBlur('status', event) }>
              <option value="fresh">Fresh</option>
              <option value="sold">Sold out!</option>
            </select>
          </div>
          <div className="row">
            <input className="col desc col-md-12" value={this.state.desc}  onBlur={ (event) => this.onBlur('desc', event) } />
          </div>
          <div className="row">
            <input className="col desc col-md-12" value={this.state.image} onBlur={ (event) => this.onBlur('image', event) }  />
          </div>
          <div className="row">
            <button className="col desc col-md-12" onClick={ () => this.onAddButtonClick() }> ADD FISH </button>
          </div>

        </div>
      );
    // });
  }



  render(){
    return (
      <div className="create-fish">
        {this.renderNewItemForm()}
      </div>
    );
  }
}

function mapStateToProps(state){
  return {

  };
}

function mapDispatchToProps(dispatch){

  return bindActionCreators({
    addFish: Actions.addFish
   }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(CreateFish);
