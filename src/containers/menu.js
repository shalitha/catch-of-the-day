import React, {Component}  from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as Actions from '../actions/index';

require('../../style/menu.scss');

class Menu extends Component{
  fishes = [];

  constructor(props){
    super(props);

    Actions.fetchFishes();
  }

  renderTitle(){
    return (
      <div className="title">
        <h1>Catch <span className="ofThe">of the</span> day</h1>
        <h3>Fresh seafood market</h3>
      </div>
    );
  }

  renderMenuItems(){
    return this.props.fishes.items.map( fish => {
      return (
        <div
        key={fish.name}
        className="item">
          <div className="row">
            <div className="col col-md-3">
              <div className="fish-image">
                  <img className="img-responsive img-thumbnail center-block" src={fish.image} />
              </div>

            </div>
            <div className="col col-md-9 padding-left">
              <div className="row">
                <div className="col col-md-10"><h3>{fish.name}</h3></div>
                <div className="col col-md-2"><p>{fish.price}</p></div>
              </div>
              <div className="row">
                <div className="col"><p>{fish.desc}{fish.status}</p></div>
              </div>
              <div className="row">
                <div className="col">
                  <button onClick={ () => this.props.addToOrder(fish) } disabled={fish.status == 'sold'}>
                    {fish.status == 'sold'? 'Sold Out!' : 'ADD TO ORDER' }
                  </button>
                </div>
              </div>
            </div>
          </div>

        </div>
      );
    } );
  }

  render(){
    return (
      <div className="menu col-md-5">
        {this.renderTitle()}
        {this.renderMenuItems()}
      </div>
    );
  }
}

function mapStateToProps(state){
  return {
    fishes: state.fishes
  };
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    addToOrder: Actions.addFishToOrder
   }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Menu);
