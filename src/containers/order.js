import React, {Component}  from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as Actions from '../actions/index';

require('../../style/order.scss');

class Order extends Component{
  renderCurrency(amount, symbol = '$'){
    return symbol + (amount / 100)
  }

  renderQty(qty, symbol = 'lbs'){
    return symbol + ' ' + qty;
  }

  renderTitle(){
    return (
      <div className="title">
        <h1>Your order</h1>
      </div>
    );
  }

  renderList(){
    // console.log(this.props);
    return _.map(this.props.order.items, fish => {
      return (
        <tr key={fish.id}>
          <td>{this.renderQty(fish.qty)}</td>
          <td>{fish.name}</td>
          <td>{this.renderCurrency(fish.subTotal)}</td>
        </tr>
      );
    });
  }

  render(){
    return (
      <div className="col-md-3 order">
        {this.renderTitle()}
        <table className="table table-condensed">
            <tbody>
              {this.renderList()}
            </tbody>
        </table>

      </div>
    );
  }
}

function mapStateToProps({order}){
  // console.log(state);
  return {
    order
  };
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({
    // addToOrder: Actions.addFishToOrder
   }, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(Order);
