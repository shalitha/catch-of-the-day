import * as types from '../constants/types';
import fishData from '../reducers/fishes';

export function removeFish(fish){
  return {
    type: types.DELETE_FISH,
    payload: fish
  };
}

export function addFish(fish){
  return {
    type: types.ADD_FISH,
    payload: fish
  };
}

export function editFish(fish){
  return {
    type: types.EDIT_FISH,
    payload: fish
  };
}

export function selectFish(fish){
  return {
    type: types.SELECT_FISH,
    payload: fish
  };
}

export function fetchFishes(){
  return {
        type: types.FETCH_FISHES,
        fishes: fishData
      }
}

export function fetchOrder(){
  return {
        type: types.FETCH_ORDER
      }
}

export function addFishToOrder(fish){
  return {
    type: types.ADD_FISH_TO_ORDER,
    payload: fish
  };
}
