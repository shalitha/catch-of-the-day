export const FETCH_FISHES = 'FETCH_FISHES';
export const FETCH_ORDER = 'FETCH_ORDER';

export const ADD_FISH = 'ADD_FISH';
export const ADD_FISH_TO_ORDER = 'ADD_FISH_TO_ORDER';
export const DELETE_FISH = 'DELETE_FISH';
export const EDIT_FISH = 'EDIT_FISH';
export const SELECT_FISH = 'SELECT_FISH';
