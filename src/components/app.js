import React, { Component } from 'react';

import Menu from '../containers/menu';
import Order from '../containers/order';
import Inventory from '../containers/inventory';

require('./main.scss');

export default class App extends Component {
  render() {
    return (
      <div className="app row">
        <Menu />
        <Order />
        <Inventory />
      </div>
    );
  }
}
