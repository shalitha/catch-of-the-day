 const fishData = [
    { id: 1, name: 'Pacific Halibut', price: 4324, status: 'fresh', image: 'https://i.istockimg.com/file_thumbview_approve/36248396/5/stock-photo-36248396-blackened-cajun-sea-bass.jpg', desc: 'Everyones favorite white fish. We will cut it to the size you need and ship it.' },
    { id: 2, name: 'Lobster', price: 3200, status: 'fresh', image: 'https://i.istockimg.com/file_thumbview_approve/32135274/5/stock-photo-32135274-cooked-lobster.jpg', desc: 'These tender, mouth-watering beauties are a fantastic hit at any dinner party.' },
    // { name: 'Pacific Halibut', price: 4324, status: 'fresh', image: 'https://i.istockimg.com/file_thumbview_approve/36248396/5/stock-photo-36248396-blackened-cajun-sea-bass.jpg', desc: '' },
  ];

export default fishData;

// export default fishCount = 2;
