import * as types from '../constants/types.js';
import _ from 'lodash';

export default function(state = { items: [] }, action){
// console.log(action);
  switch(action.type){
    case types.ADD_FISH_TO_ORDER:
      const index = _.findIndex(state.items, function(fish){
        return fish.id == action.payload.id;
      });

      if(index > -1){
        var f = action.payload;
        f.subTotal = f.price * ++f.qty;
        return {
          items: state.items.map(fish => fish.id === action.payload.id ? { ...fish, ...f } : fish)
        }
      } else {
        var fish = action.payload;
        fish.qty = 1;
        fish.subTotal = fish.price;
        return {
          items: [action.payload, ...state.items]
        };
      }
      break;
  }
  return state;
}
