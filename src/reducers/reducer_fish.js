import fishData from './fishes.js';

import * as types from '../constants/types.js';

const initialState = {
  items: fishData
};

const actionMap = {
    [types.FETCH_FISHES]:(state, action) => {
      console.log(action);
      return {
        items: fishData,
      };
    },
    [types.ADD_FISH]:(state, action) => {
      return {
        items: state.items.concat([action.payload])
      };
    },
    [types.EDIT_FISH]: (state, action) => {

    },
    [types.DELETE_FISH]: (state, action) => {
      return {
        items: state.items.filter(c => c.id !== action.payload.id)
      }
    },
    // [types.ADD_FISH_TO_ORDER]:(state = [], action) => {
    //   return {
    //     items: state.concat([action.payload])
    //   };
    // }
  };

export default function fishes (state = initialState, action) {
  const reduceFn = actionMap[action.type]
  if (!reduceFn) return state

  return Object.assign({}, state, reduceFn(state, action))
}
