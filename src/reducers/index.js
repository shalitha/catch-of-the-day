import { combineReducers } from 'redux';

import fishes from './reducer_fish';
import order from './reducer_order';

const rootReducer = combineReducers({
  fishes,
  order
});

export default rootReducer;
